package javamongo;

import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import static com.mongodb.client.model.Filters.eq;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import static java.util.Arrays.asList;
import java.util.Locale;
import org.apache.log4j.Logger;
import org.bson.Document;


/*
* Accessing MongoDB with Java - see MongoDB docs.
* https://docs.mongodb.org/getting-started/java/client/
*/
public class JavaMongo {

    final static Logger LOG = Logger.getLogger(JavaMongo.class);

    public static void main(String[] args) throws ParseException {

        // formatting..... 
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);
        LOG.debug("Dateformat set");

        // creating a client ...........................
        // default client is localhost, port 27017:
        MongoClient mongoClient = new MongoClient();
        LOG.debug("MongoClient created.");

        // getting the database..........................
        MongoDatabase db = mongoClient.getDatabase("test");
        LOG.debug("Using mongo database " + db.getName());

        // getting the collection of restaurants..........
        MongoCollection<Document> col = db.getCollection("restaurants");
        LOG.debug("Retrieved mongo collection restaurants");

        long count = col.count();
        LOG.debug("Restaurant document count: " + count);

        // deleting the ones we add below.................
        col.deleteMany(new Document("name", "Vella"));
        LOG.debug("Restaurant document count: " + col.count());

        // finding all or some .......................
        FindIterable<Document> all = col.find();
        LOG.debug("Returned all docs.");

        FindIterable<Document> some = col.find(new Document("address.zipcode", "10075"));
        LOG.debug("Returned some docs. ");
        print(some.first());
        LOG.debug("Printed the first. ");

        FindIterable<Document> some2 = col.find(eq("address.zipcode", "10075"));
        LOG.debug("Returned some docs.");
        some2.first().toString();
        LOG.debug("Printed the first. ");

        // foreach..........................
        //  all.forEach((Block<Document>) System.out::println);
        some.forEach((Block<Document>) System.out::println);
        LOG.debug("Used foreach to print some. ");

        // adding to a list..........
        ArrayList<Restaurant> list = new ArrayList<>();

        some.forEach((Block<Document>) d -> {

            String sid = (String) d.get("restaurant_id");
            long id = Integer.parseInt(sid);
            String b = (String) d.get("borough");
            String s = (String) d.get("cuisine");
            String n = (String) d.get("name");

            Restaurant r = new Restaurant();
            r.setBorough(b);
            r.setCuisine(s);
            r.setName(n);
            r.setRestaurant_id(id);
            list.add(r);
            LOG.info(r);

        });
        int n = list.size();
        LOG.debug("We now have a list of " + n + " restaurant objects from the given zipcode. ");

        LOG.debug("Total restaurant document count: " + count);
        db.getCollection("restaurants").insertOne(
                new Document("address",
                        new Document()
                        .append("street", "2 Avenue")
                        .append("zipcode", "10075")
                        .append("building", "1480")
                        .append("coord", asList(-73.9557413, 40.7720266)))
                .append("borough", "Manhattan")
                .append("cuisine", "Italian")
                .append("grades", asList(
                                new Document()
                                .append("date", format.parse("2014-10-01T00:00:00Z"))
                                .append("grade", "A")
                                .append("score", 11),
                                new Document()
                                .append("date", format.parse("2014-01-16T00:00:00Z"))
                                .append("grade", "B")
                                .append("score", 17)))
                .append("name", "Vella")
                .append("restaurant_id", "41704620"));

        long newCount = col.count();
        LOG.debug("New restaurant document count: " + newCount);
    }

    private static void print(final Document d) {
        System.out.println("Details:");
        System.out.println("========");
        d.entrySet().stream().forEach(each -> {
            System.out.println("Key : " + each.getKey());
            System.out.println("Value : " + each.getValue());
            System.out.println();

        });
    }
}
