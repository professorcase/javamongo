package javamongo;

public class Restaurant {

    public Restaurant(long restaurant_id, String name, String borough, String cuisine) {
        this.restaurant_id = restaurant_id;
        this.name = name;
        this.borough = borough;
        this.cuisine = cuisine;
    }

    private long restaurant_id;

    Restaurant() {
       
    }

    public void setRestaurant_id(long restaurant_id) {
        this.restaurant_id = restaurant_id;
    }
    private String name;
    private String borough;

    @Override
    public String toString() {
        return "Restaurant{" + "restaurant_id=" + restaurant_id + ", name=" + name + ", borough=" + borough + ", cuisine=" + cuisine + '}';
    }
    private String cuisine;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBorough() {
        return borough;
    }

    public void setBorough(String borough) {
        this.borough = borough;
    }

    public String getCuisine() {
        return cuisine;
    }

    public void setCuisine(String cuisine) {
        this.cuisine = cuisine;
    }

    public long getRestaurant_id() {
        return restaurant_id;
    }

}
